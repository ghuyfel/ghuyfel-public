/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0
import bb.data 1.0

Page {
    onCreationCompleted: {
        dataSource.load();
    }
    attachedObjects: [
        ComponentDefinition {
            id: option 
            Option {
                 
            }  
        },
        DataSource {
            id: dataSource
            type: DataSourceType.Json
            source: app.getValueFor("source", "").toString()
            
            onDataLoaded: {
                drp.removeAll();
                
                for(var i in data){
                    
                    var t =  data[i]
//                    console.log("i: " + t["option"]);
//                    console.log("data[i]: " + data[i].option);
                    
                    var o = option.createObject();
                    o.text = t["option"];
                    drp.insert(i, o);
                   
                }
            }
            
            onError: {
                console.log("Error with datasource: " + errorMessage);
            }
        }
    ]
    Container {
        Label {
            // Localized text with the dynamic translation and locale updates support
            text: qsTr("Enter the option to add") + Retranslate.onLocaleOrLanguageChanged
            textStyle.base: SystemDefaults.TextStyles.BigText
        }
        
        TextArea {
            id: opt
            
            
            onTextChanging: {
                save.enabled = text.length > 0;
            }
            
        }
        Button {
            id: save
            horizontalAlignment: HorizontalAlignment.Fill
            text: "save"
            enabled: false
            onClicked: {
                app.addOption(opt.text);
                dataSource.load();
            }
        }
        DropDown {
            id: drp
            title: "Select option"
            
            options: [
               
            ]
        }
    }
}
